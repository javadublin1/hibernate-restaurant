package com.jdub1;

import java.util.Scanner;

public class MainScannerWrong {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Linia 1:");
        System.out.println(scanner.nextLine());

        System.out.println("Linia 2:");
        System.out.println(scanner.next());

        System.out.println("Linia 3:");
        System.out.println(scanner.nextLine());

        System.out.println("Linia 4:");
        System.out.println(scanner.nextDouble());
        scanner.nextLine();

        System.out.println("Linia 5:");
        System.out.println(scanner.nextLine());
    }
}
