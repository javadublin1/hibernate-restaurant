package com.jdub1.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

/*
 * Składamy zamówienie w restauracji.
 *
 * Na zamówieniu jest informacja "co zamawiamy" - jedno pole z opisem zamówienia.
 * Dodajemy godzinę złożenia zamówienia, oraz godzinę dostarczenia zamówienia.
 *
 * W zamówieniu musi być podana ilość (ile osób) oraz numer stolika.
 *
 * Kwota zapłacona.
 * Kwota rachunku.
 * */
@Data // EqualsHashCode // ToString // Getter //Setter ...
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "rOrder")
public class Order implements IBaseEntity{
    @Id
    // identity - baza przydziela identyfikator, wstawiamy rekord i sprawdzamy id ktore zostalo wstawione
    // sequence - hibernate sequence - pobierz id, przypisz do obiektu, następnie wstaw obiekt do bazy
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    private String description; // co zamawiamy.
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
    private Set<Product> products;

    @CreationTimestamp
    private LocalDateTime timeOrdered; // moment dodania rekordu do bazy, to moment wstawienia wartości
    private LocalDateTime timeDelivered; // moment dostarczenia zamówienia.

    private int peopleCount;

    private int tableNumber;

    @Formula("(select sum(p.value*p.amount) from Product p where p.order_id = id)")
    private Double toPay; // to nie jest kolumna
    private Double paid;  // jeśli null, to jeszcze(mamy nadzieje) nie zapłacono
}
