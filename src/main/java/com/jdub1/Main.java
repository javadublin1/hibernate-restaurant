package com.jdub1;

import com.jdub1.db.EntityDao;
import com.jdub1.db.HibernateUtil;
import com.jdub1.model.Order;
import com.jdub1.model.Product;
import com.jdub1.service.OrderService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        OrderService orderService = new OrderService();

        boolean isWorking = true;
        do {
            String komenda = scanner.nextLine();

            if (komenda.equalsIgnoreCase("dodaj")) {
                Order order = new Order();
//        1. Napisać scanner który pozwala dodawać nowe zamówienie:
                //              - ilość osób
                System.out.println("Podaj ilość osób:");
                int ilosc = Integer.parseInt(scanner.nextLine());
                order.setPeopleCount(ilosc);

                //              - table number
                System.out.println("Podaj numer stolika:");
                int stolik = Integer.parseInt(scanner.nextLine());
                order.setTableNumber(stolik);

                //              - to pay
//                System.out.println("Podaj kwote do zapłaty:");
//                double kwota = Double.parseDouble(scanner.nextLine());
//                order.setToPay(kwota);

                orderService.add(order);
            } else if (komenda.startsWith("list")) {

                orderService.findAll().forEach(System.out::println);

            } else if (komenda.startsWith("dostarczono")) {
                System.out.println("Podaj identyfikator zamowienia:");
                Long id = Long.parseLong(scanner.nextLine());

                orderService.delivered(id);
            } else if (komenda.startsWith("zaplacono")) {
//              Do opracowania:
//                do konsoli wpisujemy identyfikator zamówienia oraz kwotę zapłaconą
                System.out.println("Podaj identyfikator zamowienia:");
                Long id = Long.parseLong(scanner.nextLine());

//                System.out.println("Podaj kwotę zapłaconą:");
//                Double kwota = Double.parseDouble(scanner.nextLine());

//                orderService.paid(id, kwota);
                orderService.paid(id);
            } else if (komenda.startsWith("dodaj_produkt")) { // mamy order, dodajemy do niego produkt

                System.out.println("Podaj identyfikator zamówienia:");
                Long idOrder = Long.parseLong(scanner.nextLine());

                Product product = new Product();

                System.out.println("Podaj ilość:");
                int ilosc = Integer.parseInt(scanner.nextLine());
                product.setAmount(ilosc);

                System.out.println("Podaj opis:");
                String opis = scanner.nextLine();
                product.setDescription(opis);

                System.out.println("Podaj wartość:");
                double kwota = Double.parseDouble(scanner.nextLine());
                product.setValue(kwota);

                orderService.addProduct(idOrder, product);
            } else if (komenda.startsWith("usun_produkt")) {

                System.out.println("Podaj identyfikator zamówienia:");
                Long idOrder = Long.parseLong(scanner.nextLine());

                Optional<Order> optionalOrder = orderService.findById(idOrder);
                if (optionalOrder.isPresent()) {
                    Order wypakowanyOrder = optionalOrder.get();

                    // wypisz wszystkie produkty na tym orderze.
                    wypakowanyOrder.getProducts().forEach(System.out::println);

                    System.out.println("Podaj identyfikator produktu:");
                    Long idProduct = Long.parseLong(scanner.nextLine());

                    orderService.removeProduct(idOrder, idProduct);
                }

            } else if (komenda.startsWith("show_undelivered")) {
                orderService.listUndelivered().forEach(System.out::println);
            } else if (komenda.startsWith("show_unpaid")) {
                orderService.listUnpaid().forEach(System.out::println);
            } else if (komenda.startsWith("sum_payments")) {
                System.out.println("Suma rachunków: " + orderService.sumPaid());
            } else if (komenda.startsWith("quit")) {
                isWorking = false;
            }
        } while (isWorking);
    }
}

// 1. pełne zarządzanie produktem (listuj produkty z orderu o id X, usun produkt z orderu o id X)
// 2. Formula (adnotacja z podzapytaniem która sumuje wartości z relacji)
// 3. listuj ordery które są:
//      - undelivered
//      - unpaid (niezapłacone)






